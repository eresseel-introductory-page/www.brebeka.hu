#!/bin/sh

mkdir -p ./output/css
mkdir -p ./output/js
mkdir -p ./output/html

JS=$(find ./assets/js/ -name "*.js" | cut -d"/" -f4)
CSS=$(find ./assets/css/ -name "*.css" | cut -d"/" -f4)
minify ./index.html >> ./output/html/index.html
minify ./404.html >> ./output/html/404.html

for i in ${JS}; do
  echo ${i}
  minify ./assets/js/${i} >> ./output/js/${i}
done

for i in ${CSS}; do
  echo ${i}
  minify ./assets/css/${i} >> ./output/css/${i}
done

rm -rf ./assets/css/* ./assets/js/*
cp -R ./output/css/* ./assets/css/
cp -R ./output/js/* ./assets/js/    
cp -R ./output/html/* .

echo "DONE"